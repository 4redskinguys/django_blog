from django.conf.urls import include, url, patterns
from django.contrib import admin
from django_blog import settings
from blog_user.views import register as register_view
from blog_user.views import login as login_view
from blog_user.views import logout as logout_view


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('news.urls')),
    url(r'^register/$', register_view, name='register'),
    url(r'^login/$', login_view, name='login'),
    url(r'^logout/$', logout_view, name='logout'),
]
urlpatterns += patterns('', (
    r'^static/(?P<path>.*)$',
    'django.views.static.serve',
    {'document_root': settings.STATIC_ROOT}
))
urlpatterns += patterns('', (
    r'^media/(?P<path>.*)$',
    'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT}
)
)
