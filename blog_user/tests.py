# coding: utf-8
from django.test import TestCase, Client
from django.utils import timezone
from blog_user.models import CustomUser
from blog_user.views import KEY as cookie_author_key
from news.models import News


class Creator(object):
    """just creator for some entities"""

    @classmethod
    def create_user(cls):
        """create user; data from the mind"""
        username, pw, email = 'ololosha111', 'ololosha123', 'hello@world.ru'
        CustomUser(login_name=username, password=pw, email=email).save()

    @classmethod
    def create_news(cls):
        """create new_post; data from the mind"""
        News(news_name='hello', create_date=timezone.now(), description="""
                ololosha ololosha ololosha ololosha ololosha
                """).save()


class LoginFormTests(TestCase):
    """forms.py -> LoginForm testing"""

    def test_redirect_after_normal_login(self):
        """check if url redirect works fine after normal loginning"""
        username, pw = 'ololosha111', 'ololosha123'
        Creator.create_user()
        Creator.create_news()
        response = Client().post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        self.assertIn('/news/1', response.content)
        self.assertEqual(response.resolver_match.func.__name__, 'index')

    def test_try_login_unregistered_before(self):
        """check if user is trying login, but he haven't been registered
        before; because one name only"""
        username, pw = 'ololosha111', 'ololosha123'
        response = Client().post('/login/', {
            'login_name': username,
            'password': pw
        })
        self.assertIn('Register, please.', response.content)
        self.assertEqual(response.resolver_match.func.__name__, 'login')

    def test_try_login_with_wrong_password(self):
        username, pw = 'ololosha111', 'ololosha123'
        Creator.create_user()   # create with username:pw attributes

        pw += 'hello'           # change pass to non-existing
        response = Client().post('/login/', {
            'login_name': username,
            'password': pw
        })
        self.assertEqual(response.resolver_match.func.__name__, 'login')
        self.assertIn('Wrong password', response.content)


class RegisterFormTests(TestCase):
    """forms.py -> RegisterForm testing"""

    def test_try_register_again_with_same_email(self):
        """checks if yet another user can't register account with the same
        email"""
        username, pw, email = 'ololosha111', 'ololosha123', 'hello@world.ru'
        Creator.create_user()
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        })
        asserting = (lambda: self.assertIn('exists already', response.content))
        asserting()
        self.assertEqual(response.resolver_match.func.__name__, 'register')

        # try another one
        response = Client().post('/register/', {
            'login_name': username + '111',
            'password': pw + '123',
            'confirm_password': pw + '123',
            'email': email
        })
        asserting()
        self.assertEqual(response.resolver_match.func.__name__, 'register')

    def test_redirect_after_normal_register(self):
        """check if url redirect works fine after normal register stage"""
        username, pw, email = 'ololosha111', 'ololosha123', 'hello@world.ru'
        Creator.create_news()
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        }, follow=True)
        self.assertIn('/news/1', response.content)
        self.assertEqual(response.resolver_match.func.__name__, 'index')

    def test_try_pass_empty_username_field(self):
        username, pw, email = '', 'ololosha123', 'hello@world.ru'
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        })
        self.assertEqual(response.resolver_match.func.__name__, 'register')
        self.assertIn('Field is required', response.content)

    def test_try_pass_empty_password_field(self):
        username, pw, email = 'ololosha111', '', 'hello@world.ru'
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        })
        self.assertEqual(response.resolver_match.func.__name__, 'register')
        self.assertIn('Field is required', response.content)

    def test_try_pass_empty_confirm_pw_field(self):
        username, pw, email = 'ololosha111', 'ololosha123', 'hello@world.ru'
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': '',
            'email': email
        })
        self.assertEqual(response.resolver_match.func.__name__, 'register')
        self.assertIn('Field is required', response.content)

    def test_try_pass_empty_email_field(self):
        username, pw, email = 'ololosha111', 'ololosha123', ''
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        })
        self.assertEqual(response.resolver_match.func.__name__, 'register')
        self.assertIn('Field is required', response.content)


class RegisterViewTests(TestCase):
    """views.py -> register view testing"""
    def test_author_in_answer_page(self):
        username, pw, email = 'ololosha111', 'ololosha123', 'hello@world.ru'
        response = Client().post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        }, follow=True)
        self.assertEqual(response.resolver_match.func.__name__, 'index')
        self.assertIn('Logout', response.content)

    def test_author_cookies(self):
        username, pw, email = 'ololosha111', 'ololosha123', 'hello@world.ru'
        client = Client()
        client.post('/register/', {
            'login_name': username,
            'password': pw,
            'confirm_password': pw,
            'email': email
        }, follow=True)
        self.assertIn('author', client.cookies)


class LoginViewTests(TestCase):
    """views.py -> login view testing"""
    def test_author_in_answer_page(self):
        Creator.create_user()
        username, pw = 'ololosha111', 'ololosha123'
        response = Client().post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        self.assertEqual(response.resolver_match.func.__name__, 'index')
        self.assertIn('Logout', response.content)

    def test_author_cookies(self):
        Creator.create_user()
        username, pw = 'ololosha111', 'ololosha123'
        client = Client()
        client.post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        self.assertIn(cookie_author_key, client.cookies)


class LogoutViewTests(TestCase):
    """views.py -> logout view testing"""
    def test_author_cookie_free(self):
        Creator.create_user()
        client = Client()
        username, pw = 'ololosha111', 'ololosha123'
        client.post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        self.assertIn(cookie_author_key, client.cookies)
        client.post('/logout/', follow=True)
        self.assertTrue(bool(client.cookies.get('author', '')))

    def test_logined_get_request(self):
        Creator.create_user()
        client = Client()
        username, pw = 'ololosha111', 'ololosha123'
        client.post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        response = client.get('/logout/', follow=True)
        self.assertEqual(response.resolver_match.func.__name__, 'index')

    def test_unlogined_get_request(self):
        Creator.create_user()
        response = Client().get('/logout/', follow=True)
        self.assertEqual(response.resolver_match.func.__name__, 'index')

    def test_logout_from_addnews_page(self):
        Creator.create_user()
        client = Client()
        client.get('/add_news/', follow=True)
        response = client.get('/logout/', follow=True)
        self.assertNotIn(cookie_author_key, client.cookies)
        self.assertEqual(response.resolver_match.func.__name__, 'index')

    def test_logined_post_request(self):
        Creator.create_user()
        username, pw = 'ololosha111', 'ololosha123'
        client = Client()
        client.post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        response = client.post('/logout/', follow=True)
        self.assertEqual(response.resolver_match.func.__name__, 'index')

    def test_unlogined_post_request(self):
        client = Client()
        response = client.post('/logout/', follow=True)
        self.assertEqual(response.resolver_match.func.__name__, 'index')
        self.assertIn('Login', response.content)
