# coding: utf-8
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse

from blog_user.forms import RegisterUserForm, LoginForm
from blog_user.models import CustomUser
from news.models import News

# COOKIE EXPARATION OPTIONS (author, his name, expire_time_seconds)
KEY, VALUE, MAX_ALIVE = 'author', str(), 60 * 3


def register(request):
    """register new user here

    :param request: expected with POST data
    """
    if KEY in request.COOKIES:
        return redirect(reverse('index'), {
            'news': News.objects.all(),
            'author': request.COOKIES.get(KEY)
        })

    form = RegisterUserForm()
    if request.method == 'POST':
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = CustomUser()
            user.login_name = request.POST['login_name']
            user.password = request.POST['password']
            user.email = request.POST['email']
            user.save()

            context = {'news': News.objects.all(), 'author': user.login_name}
            response = redirect(reverse('index'), context)
            response.set_cookie(KEY, user.login_name, max_age=MAX_ALIVE)
            return response
    return render(request, 'blog_user/register.html', {'form': form})


def login(request):
    """hadler for login action

    :param request: POST data expected
    """
    if KEY in request.COOKIES:
        return redirect(reverse('index'), {
            'news': News.objects.all(),
            'author': request.COOKIES.get(KEY)
        })

    form = LoginForm()
    render_login_page = (lambda: render(request, 'blog_user/login.html',
                                        {'form': form}))

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            context = {
                'news': News.objects.all(),
                'author': request.POST['login_name']
            }
            response = redirect(reverse('index'), context)
            response.set_cookie(KEY, request.POST['login_name'],
                                max_age=MAX_ALIVE)
            return response
    return render_login_page()


def logout(request):
    """handler for logout action

    :param request: default request object, GET method required
    """
    start_page = reverse('index')
    if request.method == 'GET':
        response = redirect(start_page, {'news': News.objects.all()})
        if KEY not in request.COOKIES:
            return response
        response.delete_cookie('author')
        return response

    context = {'news': News.objects.all()}
    if KEY in request.COOKIES:
        context.update({'author': request.COOKIES.get(KEY)})
    return redirect(start_page, context)
