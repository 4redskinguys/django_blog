# coding: utf-8
from django.db import models


class CustomUser(models.Model):
    """One blog's user"""
    email = models.EmailField('Email', max_length=20, unique=True,
                              primary_key=True)
    login_name = models.CharField('Login', max_length=30, blank=False)
    password = models.CharField('Password', max_length=20, blank=False)
