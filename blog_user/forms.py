# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from blog_user.models import CustomUser


class RegisterUserForm(forms.ModelForm):
    """Form for registration new user"""
    login_name = forms.RegexField(
                            regex=r'[a-zA-Z0-9]',
                            widget=forms.TextInput(attrs=dict(
                                  required=True,
                                  max_length=30,
                                  pattern='[a-zA-Z0-9]{3,}'
                            )),
                            label=_('Username:'),
                            error_messages={
                                'invalid': _('Letters and digits only \
                                             please.'),
                                'required': _('Field is required. \
                                              Fill it, please.')
                            })

    email = forms.EmailField(
                        widget=forms.EmailInput(attrs=dict(
                            required=True,
                            max_length=20
                        )),
                        label=_('Email:'),
                        error_messages={
                            'invalid': _('Field is incorrent. Try again, \
                                         please'),
                            'required': _('Field is required. Fill it, \
                                          please')
                        })

    password = forms.RegexField(
                        regex=r'[a-zA-Z0-9!_]',
                        widget=forms.PasswordInput(attrs={
                            'required': True,
                            'max_length': 20,
                            'pattern': r'[a-zA-Z0-9!_]{8,}'
                        }),
                        label=_('Password:'),
                        error_messages={
                            'invalid': _('Field is incorrent. Try again, \
                                         please'),
                            'required': _('Field is required. Fill it, \
                                          please')
                        })

    confirm_password = forms.RegexField(
                            regex=r'[a-zA-Z0-9!_]',
                            widget=forms.PasswordInput(attrs={
                                'required': True,
                                'max_length': 20,
                                'pattern': r'[a-zA-Z0-9!_]{8,}'
                            }),
                            label=_('Confirm password:'),
                            error_messages={
                                'invalid': _('Field is incorrent. Try again, \
                                             please'),
                                'required': _('Field is required. Fill it, \
                                              please')
                            })

    class Meta:
        model = CustomUser
        fields = ('login_name', 'email', 'password', 'confirm_password',)

    def clean_login_name(self):
        loginname = self.cleaned_data['login_name']
        if CustomUser.objects.filter(login_name=loginname):
            raise ValidationError(_('Username called `%s` exists already. Try \
                                    another one' % loginname))
        return loginname

    def clean_email(self):
        cleaned_email = self.cleaned_data['email']
        if CustomUser.objects.filter(email=cleaned_email):
            raise ValidationError(_('Username with `%s` exists already. Try \
                                    another one' % cleaned_email))
        return cleaned_email

    def clean(self):
        """detecting both passwords are equal"""
        cleaned_data = super(RegisterUserForm, self).clean()

        real_password = cleaned_data.get('password', '')
        confirmed_password = cleaned_data.get('confirm_password', '')
        passwords_exist = real_password and confirmed_password
        if passwords_exist:
            if real_password != confirmed_password:
                raise ValidationError(_('Two passwords are not the same.'))
        else:
            raise ValidationError(_('Fill password fields, please.'))
        return self.cleaned_data


class LoginForm(forms.ModelForm):
    """Provides user login form"""
    login_name = forms.RegexField(
                            regex=r'[a-zA-Z0-9]',
                            widget=forms.TextInput(attrs=dict(
                                  required=True,
                                  max_length=30
                            )),
                            label=_('Username:'),
                            error_messages={
                                'invalid': _('Letters and digits only, \
                                             please.'),
                                'required': _('Field is required. \
                                              Fill it, please.')
                            })

    password = forms.RegexField(
                        regex=r'[a-zA-Z0-9!_]',
                        widget=forms.PasswordInput(attrs={
                            'required': True,
                            'max_length': 20
                        }),
                        label=_('Password:'),
                        error_messages={
                            'invalid': _('Field is incorrent. Try again, \
                                         please'),
                            'required': _('Field is required. Fill it, \
                                          please')
                        })

    def clean_login_name(self):
        cleaned_name = self.cleaned_data['login_name']
        if not CustomUser.objects.filter(login_name=cleaned_name):
            raise ValidationError(_('`%s` isn\'t registered \
                                    yet. Register, please.' % cleaned_name))
        return cleaned_name

    def clean_password(self):
        cleaned_pw = self.cleaned_data['password']
        if not CustomUser.objects.filter(password=cleaned_pw):
            raise ValidationError(_('Wrong password. Be sure \
                                    `Caps Lock` is turned off.'))
        return cleaned_pw

    def clean(self):
        super(LoginForm, self).clean()
        cleaned_data = self.cleaned_data
        username = cleaned_data.get('login_name', '')
        pw = cleaned_data.get('password', '')
        db_user = None
        try:
            db_user = CustomUser.objects.filter(login_name=username)[0]
        except IndexError:
            raise ValidationError(_('No entity is found with username: \
                                    %s' % username))
        if db_user.password != pw:
            raise ValidationError(_('Wrong password. Be sure \
                                    `Caps Lock` is turned off.'))
        return cleaned_data

    class Meta:
        model = CustomUser
        fields = ('login_name', 'password',)
