# coding: utf-8
from django.contrib import admin
from blog_user.models import CustomUser


admin.site.register(CustomUser)
