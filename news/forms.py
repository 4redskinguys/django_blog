# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
# from datetime import datetime
from news.models import Comment
from news.models import News
from blog_user.models import CustomUser


class NewsAddForm(forms.Form):
    """Form for add news"""
    news_name = forms.RegexField(
        regex=ur'^[а-яА-ЯёЁa-zA-Z]+[а-яА-ЯёЁa-zA-Z0-9]',
        widget=forms.TextInput(attrs=dict(
            max_length=80
        )),
        label=_('News name:'),
        error_messages={
            'invalid': _('Letters and digits only, \
                                             please.'),
            'required': _('Newsname cannot be empty.')
        })

    description = forms.CharField(
        widget=forms.Textarea(attrs=dict(
            max_length=5000,
        )),
        label=_('Description:'),
        error_messages={
            'required': _('Description cannot be empty.')
        })

    picture = forms.ImageField(
        required=False,
        label=_('Picture:'))


class CommentForm(forms.Form):
    comment_input = forms.CharField(
        widget=forms.Textarea(attrs=dict(
            required=True,
            max_length=200,
            rows=6,
            cols=45
        )),
        label=_('Commentbox:'),
        error_messages={
            'invalid': _('Comment is incorrent. Try again, please.'),
            'required': _('Field is required. Fill it, please.')
        })

    author_name = forms.CharField(widget=forms.HiddenInput())
    news_id = forms.IntegerField(widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super(CommentForm, self).clean()

        author_name = cleaned_data.get('author_name', '')
        if not author_name:
            raise ValidationError(_('Probably, you\'re not loged in'))
        if not CustomUser.objects.filter(login_name=author_name):
            raise ValidationError(_('Author `%s` doesn\
                                    \'t exist.' % author_name))
        news_id = cleaned_data.get('news_id', '')
        news_id_err_msg = 'Can\'t resolve problem for that news-post.'
        if not news_id:
            raise ValidationError(_(news_id_err_msg))
        if not News.objects.filter(pk=news_id):
            raise ValidationError(_(news_id_err_msg))

        comment_content = cleaned_data.get('comment_input', '')
        if len(comment_content) > 200:  # maxlength is set up in input options
            raise ValidationError(_('Your message is too long. Make it \
                                    shorter, please. Max length of comment \
                                    message is 200 symbols.'))

        return cleaned_data

    class Meta:
        model = Comment
        fields = ('comment_content',)
