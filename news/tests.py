# coding: utf-8
import datetime
from django.test import TestCase, Client
# from django.utils.unittest import skip
from django.utils import timezone

from news.forms import NewsAddForm
from news.models import News

from blog_user.tests import Creator
from blog_user.views import KEY as cookie_author_key


class Helper(object):
    """just helper for some often operations"""

    @classmethod
    def login(cls):
        """default login\pass from blog_user.tests.Creator.create_user"""
        username, pw = 'ololosha111', 'ololosha123'
        client = Client()
        response = client.post('/login/', {
            'login_name': username,
            'password': pw
        }, follow=True)
        return client, response


class CommentFormTests(TestCase):
    """forms.py -> CommentForm testing"""

    def test_try_comment_post_with_no_author(self):
        Creator.create_news()  # now we have 1 news-post with id=1
        Creator.create_user()  # now we have 1 `default` user
        client, _ = Helper.login()

        comment_content = 'hello everyone! This is my first comment'
        news_id = 1
        response = client.post('/news/1/', {
            'news_id': news_id,
            'comment_input': comment_content
        }, follow=True)
        self.assertIn('Probably, you', response.content)

    def test_try_comment_post_with_non_existing_in_DB_author(self):
        Creator.create_news()
        Creator.create_user()
        client, _ = Helper.login()

        comment_content = 'hello everyone! This is my first comment'
        news_id, author = 1, 'pasha1234RUS'
        response = client.post('/news/1/', {
            'author_name': author,
            'news_id': news_id,
            'comment_input': comment_content
        })
        self.assertIn('Author `%s` doesn' % author, response.content)

    def test_try_comment_post_with_empty_author(self):
        Creator.create_news()
        Creator.create_user()
        client, _ = Helper.login()

        comment_content = 'hello everyone! This is my first comment'
        news_id, author = 1, ''
        response = client.post('/news/1/', {
            'author_name': author,
            'news_id': news_id,
            'comment_input': comment_content
        })
        self.assertIn('Probably, you', response.content)

    def test_try_comment_post_with_no_comment_content(self):
        Creator.create_news()
        Creator.create_user()
        client, _ = Helper.login()

        news_id, author = 1, 'ololosha111'
        response = client.post('/news/1/', {
            'author_name': author,
            'news_id': news_id
        })
        self.assertIn('Field is required. Fill it, please.',
                      response.content)

    def test_try_comment_post_with_empty_comment_content(self):
        Creator.create_news()
        Creator.create_user()
        client, _ = Helper.login()

        comment_content = ''
        news_id, author = 1, 'ololosha111'
        response = client.post('/news/1/', {
            'author_name': author,
            'news_id': news_id,
            'comment_input': comment_content
        })
        self.assertIn('Field is required. Fill it, please.',
                      response.content)

    def test_try_comment_post_with_long_comment_content(self):
        Creator.create_news()
        Creator.create_user()
        client, _ = Helper.login()

        comment_content = 'hello' * 200
        news_id, author = 1, 'ololosha111'
        response = client.post('/news/1/', {
            'author_name': author,
            'news_id': news_id,
            'comment_input': comment_content
        })
        self.assertIn('Your message is too long. Make it',
                      response.content)

    def test_try_comment_post_with_no_author_cookie(self):
        Creator.create_news()
        Creator.create_user()
        client, _ = Helper.login()
        if cookie_author_key in client.cookies:
            del client.cookies[cookie_author_key]

        comment_content = 'hello everyone from Russia!'
        news_id, author = 1, 'ololosha111'
        response = client.post('/news/1/', {
            'author_name': author,
            'news_id': news_id,
            'comment_input': comment_content
        }, follow=True)
        self.assertIn('You must login before commenting posts',
                      response.content)


class AddNewsFormTests(TestCase):
    # Testing form_valid only, not a behavior.
    def test_try_insert_empty_newsname(self):
        form_data = {'news_name': '',
                     'description': 'ASdsdsdsdsd',
                     'create_date': '2015-02-02 16:32'}
        form = NewsAddForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_try_insert_incorrect_newsname(self):
        form_data = {'news_name': '^ф#ыв4 ва^',
                     'description': 'ASdsdsdsdsd',
                     'create_date': '2015-02-02 16:32'}
        form = NewsAddForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_try_create_news_with_correct_data(self):
        form_data = {'news_name': 'Хелло ворлд',
                     'description': 'ASdsdsdsdsd',
                     'create_date': '2015-02-02 16:32'}
        form = NewsAddForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_try_insert_empty_description(self):
        form_data = {'news_name': 'Хелло ворлд',
                     'description': '',
                     'create_date': '2015-02-02 16:32'}
        form = NewsAddForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_try_add_news_without_login(self):
        # Testing form behavior.
        client = Client()
        response = client.get('/add_news/', follow=True)
        self.assertNotIn('Добавление', response.content)


class EditNewsFormTests(TestCase):
    def test_try_edit_news_without_login(self):
        Creator.create_news()
        client = Client()
        response = client.get('/news/1/edit_news/', follow=True)
        self.assertIn(
            'You must login before having possibility editing news',
            response.content)

    def test_try_edit_news_less_more_15_minutes(self):
        Creator.create_user()
        client, _ = Helper.login()
        news = News()
        news.news_name = 'fffffffff'
        news.description = 'asdasdasd'
        news.create_date = datetime.datetime.now() - timezone.timedelta(minutes=30)
        news.save()
        new = News.objects.get(news_name=news.news_name)
        response = client.get('/news/{0}/edit_news/'.format(new.id),
                              follow=True)
        self.assertRedirects(response=response, expected_url='/')

    def test_try_edit_news_equal_8_minutes(self):
        Creator.create_user()
        client, _ = Helper.login()
        news = News()
        news.news_name = 'fffffffff'
        news.description = 'asdasdasd'
        news.create_date = datetime.datetime.now() - timezone.timedelta(minutes=8)
        news.save()
        new = News.objects.get(news_name=news.news_name)
        response = client.get('/news/{0}/edit_news/'.format(new.id))
        self.assertIn('Редактирование', response.content)

    def test_try_edit_news_equal_15_minutes(self):
        Creator.create_user()
        client, _ = Helper.login()
        news = News()
        news.news_name = 'fffffffff'
        news.description = 'asdasdasd'
        news.create_date = datetime.datetime.now() - timezone.timedelta(minutes=15)
        news.save()
        new = News.objects.get(news_name=news.news_name)
        response = client.get('/news/{0}/edit_news/'.format(new.id),
                              follow=True)
        self.assertRedirects(response=response, expected_url='/')
