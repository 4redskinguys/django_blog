# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.shortcuts import redirect
# from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.template import RequestContext
from django.contrib import messages
from functools import wraps as functools_wraps
from datetime import datetime, timedelta

from news.models import News
from news.models import Comment
from news.forms import NewsAddForm
from news.forms import CommentForm
from blog_user.views import KEY as cookie_key

import sys

reload(sys)
sys.setdefaultencoding('utf8')


def need_login(not_logged_msg='You should login firstly.'):
    """decorator with param

    :param not_logged_msg: message which shows it user isn't logged in yet
    """

    def real_decorator(fn):
        @functools_wraps(fn)
        def wrapped(*args, **kwargs):
            request = args[0]
            if request.COOKIES.get('author'):
                res = fn(*args, **kwargs)
                return res
            else:
                messages.add_message(request, messages.INFO,
                                     not_logged_msg)
                return redirect(reverse('login'))

        wrapped.__name__ = fn.__name__
        return wrapped

    return real_decorator


def __can_edit_news(news_id):
    """ Return True, if less than 15 minutes have passed
        from the date of the creation of the news
    """
    return True if (datetime.now() - News.objects.get(
        pk=news_id).create_date) <= timedelta(
        minutes=15) else False


def __set_author_cookie(req_obj, args_dict):
    """set arg-options if cookie is alive yet

    :param req_obj: default request as argument of view
    :param args_dict: content gonna be passed to template, dict is mutable
    """
    if cookie_key in req_obj.COOKIES:
        args_dict.update({cookie_key: req_obj.COOKIES.get(cookie_key)})


@need_login('You must login before commenting posts.')
def __add_coments_to_post_handler(request, args, news_id):
    """POST handler to 'news' view; action for adding comment to the news-post
    with id=news_id

    :param request: default view's request object
    :param args: context for template; dict i.e. mutable
    :param news_id: id of the news-post
    """
    form = CommentForm(request.POST)
    if form.is_valid():
        comment_authorname = request.POST['author_name']
        comment_content = request.POST['comment_input']
        comment_obj = Comment(author_name=comment_authorname,
                              news=News.objects.get(pk=news_id),
                              comment_content=comment_content)
        comment_obj.save()
        args['comments'].append(comment_obj)
        return redirect('news/{0}'.format(news_id), args)
    else:
        args['commentbox_form'] = form

    return render_to_response('news/news.html', args)


def index(request):
    args = {}
    args['news'] = News.objects.all()
    __set_author_cookie(request, args)
    return render_to_response('news/index.html', args)


def news(request, news_id):
    args = {
        'news': News.objects.get(pk=news_id),
        'commentbox_form': CommentForm(),
        'comments': [comment
                     for comment in
                     Comment.objects.filter(news__pk=news_id)],
        'curr_news_id': news_id,
        'can_edit': __can_edit_news(news_id)
    }
    args.update(csrf(request))
    __set_author_cookie(request, args)

    if request.method == 'POST':
        return __add_coments_to_post_handler(request, args, news_id)

    return render_to_response('news/news.html', args)


@need_login(
    'You must login before having possibility adding news.')
def add_news(request):
    args = {}
    args.update(csrf(request))
    args['form'] = NewsAddForm()
    if request.method == 'POST':
        form = NewsAddForm(request.POST, request.FILES)
        if form.is_valid():
            news_ = News()
            news_.news_name = request.POST['news_name']
            news_.description = request.POST['description']
            news_.picture = request.FILES.get('picture')
            news_.author = request.POST['author']
            news_.save()
            cookie_context = {}
            __set_author_cookie(request, cookie_context)
            return redirect('/', cookie_context)
        else:
            args['errors'] = form.errors.values()
    __set_author_cookie(request, args)
    return render_to_response('news/add_news.html', args,
                              context_instance=RequestContext(request))


@need_login(
    'You must login before having possibility editing news.')
def edit_news(request, news_id):
    args = {}
    args.update(csrf(request))
    news_ = News.objects.get(id=news_id)
    data = {
        'news_name': news_.news_name,
        'description': news_.description,
        'author': news_.author,
        'create_date': datetime.now()
    }
    args['news_id'] = news_id
    args['form'] = NewsAddForm(data)
    if __can_edit_news(news_id):
        if request.method == 'POST':
            form = NewsAddForm(request.POST, request.FILES)
            if form.is_valid():
                news_.news_name = request.POST['news_name']
                news_.description = request.POST['description']
                if request.FILES.get('picture'):
                    news_.picture = request.FILES.get('picture')
                news_.author = request.POST['author']
                news_.save()
                cookie_context = {}
                __set_author_cookie(request, cookie_context)
                return redirect('/news/{0}/'.format(news_id),
                                cookie_context)
            else:
                args['errors'] = form.errors.values()
        __set_author_cookie(request, args)
        return render_to_response('news/edit_news.html', args,
                                  context_instance=RequestContext(
                                      request))
    else:
        return redirect('index')


@need_login(
    'You must login before having possibility to browse your news.')
def my_news(request, author):
    args = {}
    args.update(csrf(request))
    args['news'] = News.objects.filter(author=author)
    __set_author_cookie(request, args)
    return render_to_response('news/my_news.html', args,
                              context_instance=RequestContext(request))


@need_login(
    'You must login before having possibility to browse your comments.')
def my_comments(request, author):
    args = {}
    args.update(csrf(request))
    args['comments'] = Comment.objects.filter(author_name=author)
    __set_author_cookie(request, args)
    return render_to_response('news/my_comments.html', args,
                              context_instance=RequestContext(request))
