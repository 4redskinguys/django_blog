# coding: utf-8

from django.db import models
from django.utils import timezone


class News(models.Model):
    news_name = models.CharField(max_length=80,
                                 blank=False,
                                 null=True
                                 )
    create_date = models.DateTimeField(blank=False,
                                       null=True,
                                       default=timezone.now)
    description = models.TextField(blank=False)
    picture = models.ImageField(blank=True,
                                null=True)
    author = models.CharField(max_length=20,
                              blank=False)

    def __str__(self):
        return self.news_name

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'
        ordering = ['-create_date', ]


class Comment(models.Model):
    comment_content = models.TextField('Comment', blank=False,
                                       max_length=200, null=False)
    news = models.ForeignKey(News)
    author_name = models.CharField('Author\'s name', max_length=30,
                                   blank=False)
    public_datetime = models.DateTimeField(blank=False, null=False,
                                           auto_now=True)

    def __str__(self):
        return "{0} by {1} at {2}".format(self.comment_content[:10],
                                          self.author_name,
                                          self.public_datetime)

