# coding: utf-8
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'news/(?P<news_id>[0-9]+)/$', views.news, name='news'),
    url(r'^add_news/$', views.add_news, name='add_news'),
    url(r'^news/(?P<news_id>[0-9]+)/edit_news/$', views.edit_news, name='edit_news'),
    url(r'^news/(?P<author>[a-zA-Z0-9]+)/$', views.my_news, name='my_news'),
    url(r'^comments/(?P<author>[a-zA-Z0-9]+)/$', views.my_comments, name='my_comments'),
]
